package appointmentworksheet.tool;

import java.util.stream.IntStream;

import org.apache.commons.lang3.RandomStringUtils;

import appointmentworksheet.geo.StateCodeUtil;
import appointmentworksheet.time.DateTimeUtil;
import appointmentworksheet.time.TimeZoneUtil;

public class AppointmentWorkSheetGenerator {

  private static void generateWorkSheet(String worksheetFilename, int lineNumber) {

    StringBuilder workSheet = new StringBuilder();

    // create columns name
    workSheet.append("createDate").append(",").append("consumerFirstName").append(",")
        .append("consumerLastName").append(",").append("consumerEmail").append(",")
        .append("consumerPhoneNumber").append(",").append("consumerZipCode").append(",")
        .append("appointmentStateCode").append(",").append("appointmentDate").append(",")
        .append("appointmentTime").append(",").append("timezone").append(",").append("agentEmail")
        .append(",").append("agentFirstName").append(",").append("agentLastName").append("\n");


    IntStream.range(0, lineNumber).forEach(line -> {
      String createDate = DateTimeUtil.randDateTimeGenerator().split(" ")[0];
      String consumerFirstName = RandomStringUtils.randomAlphabetic(4);
      String consumerLastName = RandomStringUtils.randomAlphabetic(9);
      String consumerEmail = RandomStringUtils.randomAlphanumeric(6).concat("@consumer.com");
      String consumerPhoneNumber = RandomStringUtils.randomNumeric(10);
      String consumerZipCode = RandomStringUtils.randomNumeric(5);
      String appointmentStateCode = StateCodeUtil.randomStateCode();
      String[] appointmentDateTime = DateTimeUtil.randDateTimeGenerator().split(" ");
      String appointmentDate = appointmentDateTime[0];
      String appointmentTime = appointmentDateTime[1];
      String timezone = TimeZoneUtil.randomTimezone();
      String agentEmail = RandomStringUtils.randomAlphanumeric(6).concat("@agent.com");
      // String agentFirstName = RandomStringUtils.randomAlphabetic(4);
      // String agentLastName = RandomStringUtils.randomAlphabetic(9);
      String agentFirstName = "CSR";
      String agentLastName = "004";
      workSheet.append(createDate).append(",").append(consumerFirstName).append(",")
          .append(consumerLastName).append(",").append(consumerEmail).append(",")
          .append(consumerPhoneNumber).append(",").append(consumerZipCode).append(",")
          .append(appointmentStateCode).append(",").append(appointmentDate).append(",")
          .append(appointmentTime).append(",").append(timezone).append(",").append(agentEmail)
          .append(",").append(agentFirstName).append(",").append(agentLastName).append("\n");
    });

    AppointmentWorkSheetProcessHelper.writeWorkSheet2Resource(workSheet.toString(),
        worksheetFilename);
  }


  public static void generateWorkSheet(String worksheetFilename) {
    generateWorkSheet(worksheetFilename, 5);
  }

  public static void main(String[] args) {
    AppointmentWorkSheetGenerator.generateWorkSheet("AppointmentWorkSheet.csv");
  }

}
