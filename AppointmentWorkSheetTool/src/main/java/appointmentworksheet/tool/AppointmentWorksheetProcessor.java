package appointmentworksheet.tool;

import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import appointmentworksheet.config.DatabaseConfiguration;
import appointmentworksheet.constant.StatusConstans;
import appointmentworksheet.db.PostgresUtil;
import appointmentworksheet.model.AppointmentDetail;
import appointmentworksheet.time.DateTimeUtil;
import appointmentworksheet.time.TimeZoneUtil;

/**
 * @author Jing Chen
 */
@Component
public class AppointmentWorksheetProcessor {

    private static Logger logger = Logger.getLogger(AppointmentWorksheetProcessor.class);
    private Connection conn;
    private List<AppointmentDetail> appointmentsDetail;
    private Map<String, Long> consumerPhoneIdMap;


    @Autowired
    public AppointmentWorksheetProcessor(DatabaseConfiguration dbconfig) {
        logger.info("****************** Connect with PostgreSQL DB ******************");
        this.consumerPhoneIdMap = new HashMap<>();
        if (dbconfig == null) {
            logger.error("********* Sorry, PostgreSQL database configuration null!!!");
        }
        this.conn = PostgresUtil.getConnection(dbconfig.getHost(), dbconfig.getUsername(),
                dbconfig.getPassword(), dbconfig.getSchema());
    }

    /**
     * parse Appointment Worksheet
     */
    private void parseAppointmentWorksheet(File file) {

        logger.info("enter parseAppointmentWorksheet");

        try {

            // To ignore Processing of first row
            CSVReader reader = new CSVReader(
                    new BufferedReader(
                            new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8), 1024),
                    ',', '\n', 1);

            ColumnPositionMappingStrategy<AppointmentDetail> mappingStrategy =
                    new ColumnPositionMappingStrategy<AppointmentDetail>();

            mappingStrategy.setType(AppointmentDetail.class);

            // the fields to bind do in AppointmentDetail
            String[] columns = {"createDate", "consumerFirstName", "consumerLastName", "consumerEmail",
                    "consumerPhoneNumber", "consumerZipCode", "appointmentStateCode", "appointmentDate",
                    "appointmentTime", "timezone", "agentEmail", "agentFirstName", "agentLastName"};

            mappingStrategy.setColumnMapping(columns);

            logger.info("appointmentsDetail before CsvToBean parse: " + appointmentsDetail);

            this.appointmentsDetail = new CsvToBean<AppointmentDetail>().parse(mappingStrategy, reader);

            logger.info("appointmentsDetail after CsvToBean parse: " + appointmentsDetail);

            if (this.appointmentsDetail == null || this.appointmentsDetail.isEmpty()) {
                throw new RuntimeException("There is no data.");
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    public void processCSV(String inputFileCsv, String insertConsumersSQL, String updateConsumersSQL,
                           String insertAppointmentsSQL, String status) {
        logger.info("enter processCSV(Resource worksheetRes, String insertConsumersSQL,"
                + "String updateConsumersSQL, String insertAppointmentsSQL, String status)");
        // create output files
        File insertConsumersSQLFile = new File(insertConsumersSQL);
        File updateConsumersSQLFile = new File(updateConsumersSQL);
        File insertAppointmentsSQLFile = new File(insertAppointmentsSQL);
        File statusFile = new File(status);
        File inputFile = new File(inputFileCsv);

        processCSV(inputFile, insertConsumersSQLFile, updateConsumersSQLFile, insertAppointmentsSQLFile,
                statusFile);
    }

    /**
     *
     * @param consumers
     * @param insertConsumerSQLFile
     * @param updateConsumerSQLFile
     */
    private void processCSV(File inputFile, File insertConsumersSQLFile, File updateConsumersSQLFile,
                            File insertAppointmentsSQLFile, File statusFile) {

        logger.info("enter processCSV(Resource worksheetRes, File insertConsumersSQLFile,"
                + "File updateConsumersSQLFile, File insertAppointmentsSQLFile, File statusFile)");

        StringBuilder insertConsumersSQLBuilder = new StringBuilder();
        StringBuilder updateConsumersSQLBuilder = new StringBuilder();
        StringBuilder insertAppointmentsSQLBuilder = new StringBuilder();
        StringBuilder statusReportBuilder = new StringBuilder();

        // parse worksheet and validate data
        parseAppointmentWorksheet(inputFile);

        // construct columns for statusReport.csv
        statusReportBuilder.append("createDate,").append("consumerFirstName,")
                .append("consumerLastName,").append("consumerEmail,").append("consumerPhoneNumber,")
                .append("consumerZipCode,").append("appointmentStateCode,").append("appointmentDate,")
                .append("appointmentTime,").append("timezone,").append("agentEmail,")
                .append("agentFirstName,").append("agentLastName,").append("status\n");


        for (AppointmentDetail detail : this.appointmentsDetail) {
            // validate data
            String validateMsg = AppointmentWorkSheetProcessHelper.validateAppointmentDetail(detail);
            if (!validateMsg.equals(StatusConstans.STATUS_VALIDATION_PASS.toString())) {
                statusReportBuilder.append(detail.toString()).append(",").append(validateMsg).append("\n");
                continue;
            }

            String statusMessage = StatusConstans.STATUS_VALIDATION_PASS.toString().concat(" -> ");

            Long existingLeadId;

            // replace all spaces for each fiels
            {
                // createDate
                if (StringUtils.isNotEmpty(detail.getCreateDate())) {
                    detail.setCreateDate(detail.getCreateDate().replaceAll("\\s+", ""));
                }
                //consumerFirstName
                if (StringUtils.isNotEmpty(detail.getConsumerFirstName())) {
                    detail.setConsumerFirstName(detail.getConsumerFirstName().replaceAll("\\s+", ""));
                }
                // consumerLastName
                if (StringUtils.isNotEmpty(detail.getConsumerLastName())) {
                    detail.setConsumerLastName(detail.getConsumerLastName().replaceAll("\\s+", ""));
                }
                // consumerEmail
                if (StringUtils.isNotEmpty(detail.getConsumerEmail())) {
                    detail.setConsumerEmail(detail.getConsumerEmail().replaceAll("\\s+", ""));
                }
                // consumerPhoneNumber
                if (StringUtils.isNotEmpty(detail.getConsumerPhoneNumber())) {
                    detail.setConsumerPhoneNumber(detail.getConsumerPhoneNumber().replaceAll("\\s+", ""));
                }
                // consumerZipCode
                if (StringUtils.isNotEmpty(detail.getConsumerZipCode())) {
                    detail.setConsumerZipCode(detail.getConsumerZipCode().replaceAll("\\s+", ""));
                }
                // appointmentStateCode
                if (StringUtils.isNotEmpty(detail.getAppointmentStateCode())) {
                    detail.setAppointmentStateCode(detail.getAppointmentStateCode().replaceAll("\\s+", ""));
                }
                // appointmentDate
                if (StringUtils.isNotEmpty(detail.getAppointmentDate())) {
                    detail.setAppointmentDate(detail.getAppointmentDate().replaceAll("\\s+", ""));
                }
                // appointmentTime
                if (StringUtils.isNotEmpty(detail.getAppointmentTime())) {
                    detail.setAppointmentTime(detail.getAppointmentTime().replaceAll("\\s+", ""));
                }
                // timezone
                if (StringUtils.isNotEmpty(detail.getTimezone())) {
                    detail.setTimezone(detail.getTimezone().replaceAll("\\s+", ""));
                }
                // agentEmail
                if (StringUtils.isNotEmpty(detail.getAgentEmail())) {
                    detail.setAgentEmail(detail.getAgentEmail().replaceAll("\\s+", ""));
                }
                // agentFirstName
                if (StringUtils.isNotEmpty(detail.getAgentFirstName())) {
                    detail.setAgentFirstName(detail.getAgentFirstName().replaceAll("\\s+", ""));
                }
                //agentLastName
                if (StringUtils.isNotEmpty(detail.getAgentLastName())) {
                    detail.setAgentLastName(detail.getAgentLastName().replaceAll("\\s+", ""));
                }
            }

      /*
       * process consumers
       */
            {
                try {
                    String sql = "SELECT * FROM elig_lead WHERE phone_number = ?;";
                    PreparedStatement verifyConsumer = conn.prepareStatement(sql);
                    verifyConsumer.setString(1, detail.getConsumerPhoneNumber());
                    ResultSet rs = verifyConsumer.executeQuery();
                    if (rs.next()) {
                        String oldEmail = rs.getString("email_address");
                        String oldZipcode = rs.getString("zipcode");

                        // get existing consumer lead id
                        existingLeadId = rs.getLong("id");

                        // update existing consumer
                        logger.info("existing consumer leadId: " + existingLeadId);

                        String consumerEmail = StringUtils.isEmpty(detail.getConsumerEmail()) ? oldEmail
                                : detail.getConsumerEmail();
                        String consumerZipCode = StringUtils.isEmpty(detail.getConsumerZipCode()) ? oldZipcode
                                : detail.getConsumerZipCode();

                        String updateConsumerSQL = "UPDATE elig_lead SET email_address = '" + consumerEmail
                                + "', zipcode = '" + consumerZipCode + "', name = '" + getConsumerName(detail)
                                + "', last_update_timestamp = CURRENT_TIMESTAMP " + " WHERE id = " + existingLeadId
                                + ";\n";
                        // append updateConsumerSQL
                        updateConsumersSQLBuilder.append(updateConsumerSQL);
                        statusMessage = statusMessage.concat("Update lead information successfully -> ");

                        this.consumerPhoneIdMap.put(detail.getConsumerPhoneNumber(), existingLeadId);

                    } else {
                        // lead doesn't exist in the system
                        this.consumerPhoneIdMap.put(detail.getConsumerPhoneNumber(), Long.MIN_VALUE);
                        statusMessage = statusMessage.concat("This lead doesn't exist in the system please make sure phone number is correct. -> ");
                    }
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

      /*
       * process appointments
       */
            {
                try {

                    // Calculate appointment UTC time with format: MM/DD/YYYY HH:MM
                    String localStrDateTime = DateTimeUtil.marshalDate(detail.getAppointmentDate()) + " "
                            + DateTimeUtil.marshalTime(detail.getAppointmentTime());
                    // construct localStrDateTime to UTC
                    String utcStrDateTime = DateTimeUtil.localStrDate2UTCStrDate(localStrDateTime,
                            TimeZoneUtil.getTimeZone(detail.getTimezone()));

                    // Find agent id
                    Integer agentId = AppointmentWorkSheetProcessHelper.findAgentIdWithFirstnameAndLastname(
                            this.conn, detail.getAgentFirstName(), detail.getAgentLastName());

                    // Lead (consumer) id
                    Long leadId = this.consumerPhoneIdMap.get(detail.getConsumerPhoneNumber());

                    // check if agent has an appointment at this date and time
                    String verifyAppt4AgentSQL =
                            "SELECT * FROM cap_appointments WHERE to_char(appointment_timestamp, 'MM/DD/YYYY HH24') = ? AND agent_id = ? AND status NOT IN ('CANCELED', 'COMPLETED');";

                    // check if lead (consumer) has an appointment at this date and time
                    String verifyAppt4LeadSQL =
                            "SELECT * FROM cap_appointments WHERE to_char(appointment_timestamp, 'MM/DD/YYYY HH24') = ? AND lead_id = ? AND status NOT IN ('CANCELED');";

                    PreparedStatement verifyAppt4AgentStat = conn.prepareStatement(verifyAppt4AgentSQL);
                    PreparedStatement verifyAppt4LeadStat = conn.prepareStatement(verifyAppt4LeadSQL);

                    logger.info("localStrDateTime: " + localStrDateTime);
                    logger.info("utcStrDateTime: " + utcStrDateTime);
                    logger.info("verifyAppt4AgentSQL: SELECT * FROM cap_appointments WHERE to_char(appointment_timestamp, 'MM/DD/YYYY HH24') = '"
                            + utcStrDateTime + "' AND agent_id = " + agentId + " AND status NOT IN ('CANCELED', 'COMPLETED');");

                    if (leadId > Long.MIN_VALUE) {
                        logger.info("verifyAppt4LeadSQL: SELECT * FROM cap_appointments WHERE to_char(appointment_timestamp, 'MM/DD/YYYY HH24') = '"
                                + utcStrDateTime + "' AND lead_id = " + leadId + " AND status NOT IN ('CANCELED');");
                    } else {
                        logger.info("This lead doesn't exist in the system, please make sure phone number is correct.");
                    }

                    verifyAppt4AgentStat.setString(1, utcStrDateTime);
                    verifyAppt4AgentStat.setInt(2, agentId);
                    verifyAppt4LeadStat.setString(1, utcStrDateTime);
                    verifyAppt4LeadStat.setLong(2, leadId);

                    ResultSet verifyAppt4AgentRes = verifyAppt4AgentStat.executeQuery();
                    ResultSet verifyAppt4LeadRes = verifyAppt4LeadStat.executeQuery();

                    if (verifyAppt4AgentRes.next()) {
                        // agent has an appointment at this date and time
                        statusMessage = statusMessage.concat("Agent is busy at this date and time! -> ");
                    } else if (verifyAppt4LeadRes.next()) {
                        // lead (consumer) has an appointment at this date and time
                        statusMessage = statusMessage
                                .concat("Elig Lead/Consumer has an appointment at this date and time! -> ");
                    } else {

                        if (agentId == Integer.MIN_VALUE) {
                            statusMessage = statusMessage.concat("There is no such agent please make sure agent's firstname and lastname are correct!");
                        } else {
                            // insert new appointment
                            String insertAppointmentSQL =
                                    "INSERT INTO cap_appointments (id, appointment_timestamp, status, duration, lead_id, agent_id, state_code, creation_timestamp, last_update_timestamp) values "
                                            + "( nextval('CAP_APPOINTMENTS_SEQ'), to_timestamp('" + utcStrDateTime
                                            + "', 'MM/DD/YYYY HH24:MI'), 'ASSIGNED', 60.0, " + leadId + ", " + agentId + ", '" + detail.getAppointmentStateCode()
                                            + "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);\n";
                            // append insertAppointmentSQL
                            insertAppointmentsSQLBuilder.append(insertAppointmentSQL);
                            statusMessage = statusMessage.concat("Save new appointment with agent id successfully!");
                        }
                    }
                } catch (ParseException | SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            statusMessage = detail.toString() + ", " + statusMessage;

            // append status message
            statusReportBuilder.append(statusMessage).append("\n");
        }

    /*
     * Write to file (override existing files)
     */
        try {
            FileUtils.writeStringToFile(insertConsumersSQLFile, insertConsumersSQLBuilder.toString(), StandardCharsets.UTF_8, false);
            FileUtils.writeStringToFile(updateConsumersSQLFile, updateConsumersSQLBuilder.toString(), StandardCharsets.UTF_8, false);
            FileUtils.writeStringToFile(insertAppointmentsSQLFile, insertAppointmentsSQLBuilder.toString(), StandardCharsets.UTF_8, false);
            FileUtils.writeStringToFile(statusFile, statusReportBuilder.toString(), StandardCharsets.UTF_8, false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String getConsumerName(AppointmentDetail detail) {
        String consumerFirstname = StringUtils.isEmpty(detail.getConsumerFirstName()) ? "FirstName" : detail.getConsumerFirstName().trim();
        String consumerLastname = StringUtils.isEmpty(detail.getConsumerLastName()) ? "LastName" : detail.getConsumerLastName().trim();
        return consumerFirstname + "~^" + consumerLastname;
    }

}
