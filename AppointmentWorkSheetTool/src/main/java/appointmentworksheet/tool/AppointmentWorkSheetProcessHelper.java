package appointmentworksheet.tool;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import appointmentworksheet.constant.StatusConstans;
import appointmentworksheet.model.AppointmentDetail;

public class AppointmentWorkSheetProcessHelper {

    private static Logger logger = Logger.getLogger(AppointmentWorkSheetProcessHelper.class);

    public static Integer findAgentIdWithFirstnameAndLastname(Connection conn, String firstName,
                                                              String lastName) throws SQLException {
        Integer agentId = Integer.MIN_VALUE;
        String sql = "SELECT ag.id FROM cap_agents AS ag INNER JOIN users AS u ON ag.user_id = u.id "
                + "WHERE upper(u.first_name) = upper(?) AND upper(u.last_name) = upper(?);\n";

        PreparedStatement stat = conn.prepareStatement(sql);

        stat.setString(1, firstName);
        stat.setString(2, lastName);

        ResultSet rs = stat.executeQuery();
        if (rs.next()) {
            agentId = rs.getInt(1);
        }

        return agentId;
    }

    // generate new consumer id
    public static long generateLeadId(Connection conn) throws SQLException {
        String generateLeadIdSQL = "SELECT nextval('ELIG_LEAD_SEQ');";
        PreparedStatement ps = conn.prepareStatement(generateLeadIdSQL);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            return rs.getLong(1);
        }
        throw new RuntimeException("Could not generate new eligLeadId from SELECT nextval('ELIG_LEAD_SEQ')");
    }

    public static String validateAppointmentDetail(AppointmentDetail detail) {
        if (detail == null || StringUtils.isEmpty(detail.getAppointmentDate())
                || StringUtils.isEmpty(detail.getAppointmentTime())
                || StringUtils.isEmpty(detail.getTimezone())) {
            logger.error(StatusConstans.STATUS_VALIDATION_NOT_PASS.toString());
            return StatusConstans.STATUS_INVALID_DATA.toString();
        }

        // [Error] No lead information.
        if (StringUtils.isEmpty(detail.getConsumerPhoneNumber())) {
            logger.error(StatusConstans.STATUS_VALIDATION_NOT_PASS.toString());
            return StatusConstans.STATUS_NO_LEAD.toString();
        }
        // [Error] No agent information.
        if (StringUtils.isEmpty(detail.getAgentFirstName())
                || StringUtils.isEmpty(detail.getAgentLastName())) {
            logger.error(StatusConstans.STATUS_VALIDATION_NOT_PASS.toString());
            return StatusConstans.STATUS_NO_AGENT.toString();
        }
        logger.info(StatusConstans.STATUS_VALIDATION_PASS.toString());
        return StatusConstans.STATUS_VALIDATION_PASS.toString();
    }

    // use in maven project
    public static void writeWorkSheet2Resource(String text, String filename) {
        Path file = Paths.get("src/main/resources/".concat(filename));
        List<String> texts = Arrays.asList(text);
        try {
            Files.write(file, texts, Charset.forName("UTF-8"));
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}
