package appointmentworksheet.constant;

/**
 * @Author Jing Chen
 */
public enum StatusConstans {
    STATUS_INVALID_DATA("[Error] Invalid Data."),
    STATUS_NO_LEAD("[Error] No lead information."),
    STATUS_NO_AGENT("[Error] No agent information."),
    STATUS_VALIDATION_NOT_PASS("[Error] AppointmentDetail validation not pass!"),
    STATUS_VALIDATION_PASS("[Success] AppointmentDetail validation pass!");

    private final String name;

    private StatusConstans(String s) {
        name = s;
    }

    public String toString() {
        return this.name;
    }
}