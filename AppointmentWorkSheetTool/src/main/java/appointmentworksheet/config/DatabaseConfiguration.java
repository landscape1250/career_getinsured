package appointmentworksheet.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @Author Jing Chen
 */
@Component
@ConfigurationProperties(prefix = "postgresql")
public class DatabaseConfiguration {

    private String schema;
    private String password;
    private String host;
    private String username;


    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "DatabaseProperties [schema=" + schema + ", password=" + password + ", host=" + host
                + ", username=" + username + "]";
    }

}

