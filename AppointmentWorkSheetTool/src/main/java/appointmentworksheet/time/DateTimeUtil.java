package appointmentworksheet.time;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author Jing Chen
 */
public class DateTimeUtil {

  // TimeZone
  public static final String TZ_UTC = "UTC";
  // Date time format pattern
  public static final String APPT_DATE_HOUR_MINUTE_FORMAT = "MM/dd/yyyy HH:mm";
  // DateFormat instance
  public static final DateFormat DATE_HOUR_FORMATTER = new SimpleDateFormat("MM/dd/yyyy HH");

  public static Date convertToTimeZone(Date date, String tzFrom, String tzTo) {
    return Date.from(LocalDateTime.ofInstant(date.toInstant(), ZoneId.of(tzTo))
        .atZone(ZoneId.of(tzFrom)).toInstant());
  }

  /**
   * @param localStrDate format is MM/dd/yyyy HH:mm
   */
  public static String localStrDate2UTCStrDate(String localStrDate, TimeZone localZone)
      throws ParseException {
    Date localDate = DATE_HOUR_FORMATTER.parse(localStrDate);
    Date utcDate = convertToTimeZone(localDate, localZone.getID(), TZ_UTC);
    return DATE_HOUR_FORMATTER.format(utcDate);
  }

  public static String randDateTimeGenerator() {
    long offset = Timestamp.valueOf("2016-09-01 00:00:00").getTime();
    long end = Timestamp.valueOf("2016-10-30 00:00:00").getTime();
    long diff = end - offset + 1;
    Timestamp randTimestamp = new Timestamp(offset + (long) (Math.random() * diff));
    LocalDateTime randlocalDateTime = randTimestamp.toLocalDateTime();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(APPT_DATE_HOUR_MINUTE_FORMAT);
    return formatter.format(randlocalDateTime);
  }

  // marshal date format into MM/DD/YYYY
  public static String marshalDate(String date) {
    String[] monthDayYear = date.split("/");
    if (monthDayYear.length != 3) {
      throw new RuntimeException("Date format is invalid");
    }
    String month = monthDayYear[0];
    String day = monthDayYear[1];
    String year = monthDayYear[2];

    month = month.length() < 2 ? "0".concat(month) : month;
    day = day.length() < 2 ? "0".concat(day) : day;
    year = year.length() != 4 ? "20".concat(year) : year;
    return month + "/" + day + "/" + year;
  }

  // marshal time format into HH24:MI
  public static String marshalTime(String time) {
    String[] hourMinuteSec = time.split(":");
    if (hourMinuteSec.length < 2) {
      throw new RuntimeException("Time format is invalid");
    }
    String hour = hourMinuteSec[0];
    String minute = hourMinuteSec[1];

    hour = hour.length() < 2 ? "0".concat(hour) : hour;
    minute = minute.length() < 2 ? "0".concat(minute) : minute;
    return hour + ":" + minute;
  }
}


