package appointmentworksheet.time;

import java.util.Random;
import java.util.TimeZone;


/**
 * @Author Jing Chen
 */
public class TimeZoneUtil {

    private static String[] timezones = {"EST", "CST", "MT", "PST"};

    public static TimeZone getTimeZone(String tz) {
        if (tz == null || tz.isEmpty()) {
            return null;
        }
        tz = tz.toUpperCase().trim();
        switch (tz) {
            case "EST":
                return TimeZone.getTimeZone("US/Eastern");
            case "CST":
                return TimeZone.getTimeZone("US/Central");
            case "MT":
                return TimeZone.getTimeZone("US/Mountain");
            case "PST":
                return TimeZone.getTimeZone("US/Pacific");
            default:
                return TimeZone.getTimeZone("US/Pacific");
        }
    }

    public static String randomTimezone() {
        int len = timezones.length;
        Random randomGenerator = new Random();
        return timezones[randomGenerator.nextInt(len)];

    }

}
