package appointmentworksheet.db;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;


/**
 * @Author Jing Chen
 */
public class PostgresUtil {

    private static Logger logger = Logger.getLogger(PostgresUtil.class);

    static {
        try {
            Class.forName("org.postgresql.Driver").newInstance();
            System.out.println("postgresql JDBC Driver Registered!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection(String host, String username, String password,
                                           String schema) {

        if (StringUtils.isEmpty(host) || StringUtils.isEmpty(username) || StringUtils.isEmpty(schema)
                || StringUtils.isEmpty(password)) {
            throw new RuntimeException("Database credentials missing");
        }

        String url = "jdbc:postgresql://" + host + "/" + schema;

        logger.info("************[host] " + host);
        logger.info("************[username] " + username);
        logger.info("************[password] " + password);
        logger.info("************[schema] " + schema);
        logger.info("************[url] " + url);

        Connection con = null;
        try {
            con = DriverManager.getConnection(url, username, password);

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return con;
    }

}
