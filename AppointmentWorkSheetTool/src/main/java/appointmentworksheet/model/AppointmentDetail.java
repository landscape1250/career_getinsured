package appointmentworksheet.model;


/**
 * @Author Jing Chen
 */
public class AppointmentDetail {

    private String createDate;
    private String consumerFirstName;
    private String consumerLastName;
    private String consumerEmail;
    private String consumerPhoneNumber;
    private String consumerZipCode;
    private String appointmentStateCode;
    private String appointmentDate;
    private String appointmentTime;
    private String timezone;
    private String agentEmail;
    private String agentFirstName;
    private String agentLastName;

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getConsumerFirstName() {
        return consumerFirstName;
    }

    public void setConsumerFirstName(String consumerFirstName) {
        this.consumerFirstName = consumerFirstName;
    }

    public String getConsumerLastName() {
        return consumerLastName;
    }

    public void setConsumerLastName(String consumerLastName) {
        this.consumerLastName = consumerLastName;
    }

    public String getConsumerEmail() {
        return consumerEmail;
    }

    public void setConsumerEmail(String consumerEmail) {
        this.consumerEmail = consumerEmail;
    }

    public String getConsumerPhoneNumber() {
        return consumerPhoneNumber;
    }

    public void setConsumerPhoneNumber(String consumerPhoneNumber) {
        this.consumerPhoneNumber = consumerPhoneNumber;
    }

    public String getConsumerZipCode() {
        return consumerZipCode;
    }

    public void setConsumerZipCode(String consumerZipCode) {
        this.consumerZipCode = consumerZipCode;
    }

    public String getAppointmentStateCode() {
        return appointmentStateCode;
    }

    public void setAppointmentStateCode(String appointmentStateCode) {
        this.appointmentStateCode = appointmentStateCode;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public String getAgentFirstName() {
        return agentFirstName;
    }

    public void setAgentFirstName(String agentFirstName) {
        this.agentFirstName = agentFirstName;
    }

    public String getAgentLastName() {
        return agentLastName;
    }

    public void setAgentLastName(String agentLastName) {
        this.agentLastName = agentLastName;
    }


    @Override
    public String toString() {
        return createDate +
                "," + consumerFirstName +
                "," + consumerLastName +
                "," + consumerEmail +
                "," + consumerPhoneNumber +
                "," + consumerZipCode +
                "," + appointmentStateCode +
                "," + appointmentDate +
                "," + appointmentTime +
                "," + timezone +
                "," + agentEmail +
                "," + agentFirstName +
                "," + agentLastName;
    }
}
