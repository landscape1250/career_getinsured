package boot;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

import appointmentworksheet.tool.AppointmentWorksheetProcessor;


/**
 * @Author Jing Chen
 */
@Component
@SpringBootApplication
@ComponentScan("appointmentworksheet")
public class AppointmentWorkSheetApp {

  private static Logger logger = Logger.getLogger(AppointmentWorkSheetApp.class);

  public static void main(String[] args) {

    try {
      String toolPath = new File(".").getCanonicalPath();

      String dataPath = toolPath.concat("/data/");
      String resultPath = toolPath.concat("/result/");

      logger.info("************[dataPath] " + dataPath);
      logger.info("************[resultPath] " + resultPath);

      logger.info("Starting loading process");
      ConfigurableApplicationContext context =
          SpringApplication.run(AppointmentWorkSheetApp.class, args);
      AppointmentWorksheetProcessor processor =
          context.getBean(AppointmentWorksheetProcessor.class);

      logger.info("#########[loading AppointmentWorkSheet: " + args[0] + "]#########");

      logger.info("%%%%%%%%%%%%%[AppointmentWorkSheet.csv] Path: " + dataPath + args[0]);

      // input file with "java -jar" parameter
      String worksheet = dataPath.concat(args[0]);

      // output files
      String insertConsumersSQL = resultPath.concat("insertConsumersSQL.sql");
      String updateConsumersSQL = resultPath.concat("updateConsumersSQL.sql");
      String insertAppointmentsSQL = resultPath.concat("insertAppointmentsSQL.sql");
      String status = resultPath.concat("statusReport.csv");

      logger.info("************[worksheet] " + worksheet);

      logger.info("************[insertConsumersSQL] " + insertConsumersSQL);
      logger.info("************[updateConsumersSQL] " + updateConsumersSQL);
      logger.info("************[insertAppointmentsSQL] " + insertAppointmentsSQL);
      logger.info("************[status] " + status);

      processor.processCSV(worksheet, insertConsumersSQL, updateConsumersSQL, insertAppointmentsSQL,
          status);
    } catch (IOException e) {
      throw new RuntimeException(e.getMessage());
    }
    logger.info("Completed loading process");

  }


}
