package demo;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class WriteStringToFileDemo {

  public static void main(String[] args) throws IOException, ClassNotFoundException {

    String string = "This is\na test";
    File testFile = new File("test01.txt");
    FileUtils.writeStringToFile(testFile, string, "UTF-8", true);
  }
}
