package boot;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import couchbaseload.Loader;

/**
 * @Author Jing Chen
 */
@Component
@SpringBootApplication
@ComponentScan("couchbaseload")
public class AgentGroupLoader {

  private static Logger logger = Logger.getLogger(AgentGroupLoader.class);


  public static void main(String[] args) throws IOException {

    // find jar path
    File jarPath = new File(
        AgentGroupLoader.class.getProtectionDomain().getCodeSource().getLocation().getPath());
    String dataPath = jarPath.getParent() + "/data/";
    logger.info("*******************" + dataPath + "*******************");

    logger.info("Starting loading process");
    ConfigurableApplicationContext context = SpringApplication.run(AgentGroupLoader.class, args);
    Loader loader = context.getBean("loader", Loader.class);

    Resource template = null;

    // load Data to couchbase
    logger.info("#########[loading State Group]#########");
    {
      template = context.getResource(dataPath.concat(args[0]));
      logger.info("%%%%%%%%%%%%%[" + args[0] + "] Path: " + dataPath.concat(args[0]));
      loader
          .loadStateGroupData(new BufferedReader(new InputStreamReader(template.getInputStream())));
    }

    logger.info("#########[loading Zipcodes]#########");
    {
      template = context.getResource(dataPath.concat(args[1]));
      logger.info("%%%%%%%%%%%%%[" + args[1] + "] Path: " + dataPath.concat(args[1]));
      loader.loadZipcodesData(new BufferedReader(new InputStreamReader(template.getInputStream())));
    }

    logger.info("#########[loading Zipcodes out of territory]#########");
    {
      template = context.getResource(dataPath.concat(args[2]));
      logger.info("%%%%%%%%%%%%%[" + args[2] + "] Path: " + dataPath.concat(args[2]));
      loader.loadZipcodesOutOfTerritoryData(
          new BufferedReader(new InputStreamReader(template.getInputStream())));
    }

    logger.info("Completed loading process");

  }


}
