package couchbaseload;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.log4j.Logger;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.opencsv.CSVReader;

/**
 * @Author Jing Chen
 */
public class CSVLoader {

  private static Logger logger = Logger.getLogger(CSVLoader.class);
  private CSVReader csvReader;
  private Bucket bucket;
  private String className;


  public CSVLoader(CouchbaseProperties couchbaseProperties) {
    try {
      if (couchbaseProperties == null) {
        logger.error("********* Sorry, properties null!!!");
      } else {
        logger.error("******************:" + couchbaseProperties.toString());
      }
      this.bucket = CouchbaseCluster.create(couchbaseProperties.getNodes())
          .openBucket(couchbaseProperties.getBucketName(), couchbaseProperties.getBucketPassword());
      this.className = "com.getinsured.hix.platform.ecm.couchbase.dto.CouchEcmDocument";
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * load StateGroup CSV To Couchbase
   */

  public void loadStateGroupCSV(BufferedReader br, String outFile) {
    try {
      this.csvReader = new CSVReader(br);
      String[] csvRow;
      long counter = 0;
      StringBuilder stateGroupsInsertion = new StringBuilder();
      while ((csvRow = this.csvReader.readNext()) != null) {
        ++counter;
        logger.info("@@@@@@@@@@@@@@@[loadStateGroupCSV counter]: " + counter);
        long curTime = System.currentTimeMillis();
        JsonObject object = JsonObject.create();
        String id = "StateGroup::" + csvRow[0];
        // construct JSON object to insert into bucket
        object.put("id", id).put("State", csvRow[0]).put("StateName", csvRow[1])
            .put("GroupId", csvRow[2])
            // couchbase common info
            .put("_class", this.className);
        // metaData
        JsonObject metaData = JsonObject.create();
        metaData.put("subCategory", "STGRP").put("createdBy", "migrationadmin")
            .put("modifiedDate", curTime).put("modifiedBy", "migrationadmin").put("category", "CAP")
            .put("creationDate", curTime);
        object.put("metaData", metaData);
        // create document
        JsonDocument document = JsonDocument.create(id, object);
        // stateGroup Insertion statement
        String Insertion = "UPSERT INTO " + bucket.name() + " (KEY, VALUE) VALUES (\"" + id + "\","
            + document.content().toString() + ");";
        stateGroupsInsertion.append(Insertion).append("\n");
      }
      Files.write(Paths.get(outFile), stateGroupsInsertion.toString().getBytes());
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      logger.info("##################StateGroup CSV loaded to Couchbase!##################");
    }
  }

  /**
   * load Zipcodes CSV (in production) To Couchbase
   */

  public void loadZipcodesCSV(BufferedReader br, String outFile) {
    try {
      this.csvReader = new CSVReader(br);
      String[] csvRow = null;

      long counter = 0;

      StringBuilder zipcodesInsertion = new StringBuilder();

      while ((csvRow = this.csvReader.readNext()) != null) {
        ++counter;
        logger.info("~~~~~~~~~~~~~~~~~~~[loadZipcodesCSV counter]: " + counter);

        long curTime = System.currentTimeMillis();
        JsonObject object = JsonObject.create();
        String id = "Zipcode::" + csvRow[1];
        // construct JSON object to insert into bucket
        object.put("id", id).put("ZipCode", csvRow[1]).put("City", csvRow[2])
            .put("CityType", csvRow[3]).put("County", csvRow[4]).put("CountyFIPS", csvRow[5])
            .put("StateName", csvRow[6]).put("State", csvRow[7])
            .put("State2", "StateGroup::" + csvRow[7]).put("StateFIPS", csvRow[8])
            .put("MSA", csvRow[9]).put("AreaCode", csvRow[10]).put("Timezone", csvRow[11])
            .put("GMTOffset", csvRow[12]).put("DST", csvRow[13]).put("Latitude", csvRow[14])
            .put("Longitude", csvRow[15])
            // couchbase common info
            .put("_class", this.className);
        // metaData
        JsonObject metaData = JsonObject.create();
        metaData.put("subCategory", "ZIPCODE").put("createdBy", "migrationadmin")
            .put("modifiedDate", curTime).put("modifiedBy", "migrationadmin").put("category", "CAP")
            .put("creationDate", curTime);
        object.put("metaData", metaData);
        // create document
        JsonDocument document = JsonDocument.create(id, object);

        // stateGroup Insertion statement
        String Insertion = "UPSERT INTO " + bucket.name() + " (KEY, VALUE) VALUES (\"" + id + "\","
            + document.content().toString() + ");";
        zipcodesInsertion.append(Insertion).append("\n");
      }
      Files.write(Paths.get(outFile), zipcodesInsertion.toString().getBytes());
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      logger.info("##################Zipcodes CSV loaded to Couchbase!##################");
    }
  }

  /**
   * Zipcodes out of territory CSV loaded to Couchbase
   */

  public void loadZipcodesOutOfTerritoryCSV(BufferedReader br, String outFile) {
    try {
      this.csvReader = new CSVReader(br);
      String[] csvRow = null;
      long counter = 0;

      StringBuilder zipcodesOutOfTerritoryInsertion = new StringBuilder();

      while ((csvRow = this.csvReader.readNext()) != null) {
        ++counter;
        logger.info("<<<<<<<<<<<<<<<<<<<<[loadZipcodesOutOfTerritoryCSV counter]: " + counter);
        long curTime = System.currentTimeMillis();
        JsonObject object = JsonObject.create();
        String id = "ZipcodeNotInTerritory::" + csvRow[0];
        // construct JSON object to insert into bucket
        object.put("id", id).put("ZipCode", csvRow[0]).put("CountyFips", csvRow[1])
            .put("County", csvRow[2]).put("StateName", csvRow[3]).put("State", csvRow[4])
            // couchbase common info
            .put("_class", this.className);
        // metaData
        JsonObject metaData = JsonObject.create();
        metaData.put("subCategory", "ZIPCODE_OUT_OF_TERRITORY").put("createdBy", "migrationadmin")
            .put("modifiedDate", curTime).put("modifiedBy", "migrationadmin").put("category", "CAP")
            .put("creationDate", curTime);
        object.put("metaData", metaData);
        // create document
        JsonDocument document = JsonDocument.create(id, object);

        // stateGroup Insertion statement
        String Insertion = "UPSERT INTO " + bucket.name() + " (KEY, VALUE) VALUES (\"" + id + "\","
            + document.content().toString() + ");";
        zipcodesOutOfTerritoryInsertion.append(Insertion).append("\n");
      }
      Files.write(Paths.get(outFile), zipcodesOutOfTerritoryInsertion.toString().getBytes());
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      logger.info(
          "##################Zipcodes out of territory CSV loaded to Couchbase!##################");
    }
  }

}
