package couchbaseload;

import java.io.BufferedReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author Jing Chen
 */
@Component
public class Loader {

  private CSVLoader csvLoader;

  @Autowired
  public Loader(CouchbaseProperties cbconfig) {
    this.csvLoader = new CSVLoader(cbconfig);
  }

  public void loadStateGroupData(BufferedReader reader) {
    this.csvLoader.loadStateGroupCSV(reader, "StateGroupsInsertion.n1ql");
  }

  public void loadZipcodesData(BufferedReader reader) {
    this.csvLoader.loadZipcodesCSV(reader, "ZipcodesInsertion.n1ql");
  }

  public void loadZipcodesOutOfTerritoryData(BufferedReader reader) {
    this.csvLoader.loadZipcodesOutOfTerritoryCSV(reader, "ZipcodesOutOfTerritoryInsertion.n1ql");
  }
}
