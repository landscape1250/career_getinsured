package boot;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import couchbaseload.Loader;

/**
 * @Author Jing Chen
 */
@Component
@SpringBootApplication
@ComponentScan("couchbaseload")
public class AgentGroupLoader {

    private static Logger logger = Logger.getLogger(AgentGroupLoader.class);

    public static void main(String[] args) throws IOException {

        // find jar path
        File jarPath = new File(AgentGroupLoader.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        String dataPath = jarPath.getParent() + "/data/";
        logger.info("*******************" + dataPath + "*******************");

        logger.info("Starting loading process");
        ConfigurableApplicationContext context = SpringApplication.run(AgentGroupLoader.class, args);
        Loader loader = context.getBean("loader", Loader.class);

        if (args == null || args.length == 0) {
            logger.info("Usage:\n java -jar AgentGroupCouchbaseLoaderJar*.jar StageGroup.csv ZipCode.csv ZipCodeOutOfTerritory.csv  --spring.config.location=bucket.properties\n");
            return;
        }

        Resource template = null;

        // load Data to CouchBase
        {
            if (!args[0].trim().toUpperCase().startsWith("SKIP")) {
                template = context.getResource(dataPath.concat(args[0]));
                logger.info("#########[loading State Group]#########");
                logger.info("%%%%%%%%%%%%%[State Group document] Path: " + dataPath.concat(args[0]));
                loader.loadStateGroupData(new BufferedReader(new InputStreamReader(template.getInputStream())));
            }
        }

        {
            if (!args[1].trim().toUpperCase().startsWith("SKIP")) {
                template = context.getResource(dataPath.concat(args[1]));
                logger.info("#########[loading Zipcodes]#########");
                logger.info("%%%%%%%%%%%%%[Zipcodes document] Path: " + dataPath.concat(args[1]));
                loader.loadZipcodesData(new BufferedReader(new InputStreamReader(template.getInputStream())));
            }
        }

        {
            if (!args[2].trim().toUpperCase().startsWith("SKIP")) {
                template = context.getResource(dataPath.concat(args[2]));
                logger.info("#########[loading Zipcodes out of territory]#########");
                logger.info("%%%%%%%%%%%%%[Zipcodes out of territory document] Path: " + dataPath.concat(args[2]));
                loader.loadZipcodesOutOfTerritoryData(new BufferedReader(new InputStreamReader(template.getInputStream())));
            }
        }

        logger.info("Completed loading process");

    }


}
