package couchbaseload;


import org.apache.log4j.Logger;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author Jing Chen
 */
@Component
@ConfigurationProperties(prefix = "couchbase")
public class CouchbaseProperties {

    private Logger logger = Logger.getLogger(CouchbaseProperties.class);
    private String bucketName;
    private String bucketPassword;
    private String nodes;

    public String getBucketPassword() {
        return bucketPassword;
    }

    public void setBucketPassword(String bucketPassword) {
        this.bucketPassword = bucketPassword;
    }

    public String getNodes() {
        return nodes;
    }

    public void setNodes(String nodes) {
        this.nodes = nodes;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Setting bucketName:" + bucketName);
        this.bucketName = bucketName;
    }

    @Override
    public String toString() {
        return "CouchbaseProperties{" +
                "bucketName='" + bucketName + '\'' +
                ", bucketPassword='" + bucketPassword + '\'' +
                ", nodes='" + nodes + '\'' +
                '}';
    }
}
