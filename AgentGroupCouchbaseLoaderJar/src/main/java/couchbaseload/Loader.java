package couchbaseload;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;

/**
 * @Author Jing Chen
 */
@Component
public class Loader {
    private static Logger logger = Logger.getLogger(Loader.class);

    private CSVLoader csvLoader;

    @Autowired
    public Loader(CouchbaseProperties cbconfig) {
        this.csvLoader = new CSVLoader(cbconfig);
    }

    public void loadStateGroupData(BufferedReader reader) {
        this.csvLoader.loadStateGroupCSV(reader);
    }

    public void loadZipcodesData(BufferedReader reader) {
        this.csvLoader.loadZipcodesCSV(reader);
    }

    public void loadZipcodesOutOfTerritoryData(BufferedReader reader) {
        this.csvLoader.loadZipcodesOutOfTerritoryCSV(reader);
    }
}
