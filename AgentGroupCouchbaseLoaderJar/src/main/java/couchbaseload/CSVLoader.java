package couchbaseload;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.opencsv.CSVReader;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashSet;

/**
 * @Author Jing Chen
 */
public class CSVLoader {

    private static Logger logger = Logger.getLogger(CSVLoader.class);
    private CSVReader csvReader;
    private Bucket bucket;
    private String className;


    public CSVLoader(CouchbaseProperties couchbaseProperties) {
        try {
            if (couchbaseProperties == null) {
                logger.error("********* Sorry, properties null!!!");
            } else {
                logger.info("******************:" + couchbaseProperties.toString());
                this.bucket = CouchbaseCluster.create(couchbaseProperties.getNodes()).openBucket(couchbaseProperties.getBucketName(), couchbaseProperties.getBucketPassword());
                this.className = "com.getinsured.hix.platform.ecm.couchbase.dto.CouchEcmDocument";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * load StateGroup CSV To Couchbase
     */
    public void loadStateGroupCSV(String csvFilePath) throws FileNotFoundException {
        loadStateGroupCSV(new BufferedReader(new FileReader(csvFilePath)));
    }

    public void loadStateGroupCSV(BufferedReader br) {
        // count distinct id
        HashSet<String> idSet = new HashSet<String>();

        try {
            this.csvReader = new CSVReader(br);
            String[] csvRow = null;

            long counter = 0;

            while ((csvRow = this.csvReader.readNext()) != null) {
                long curTime = System.currentTimeMillis();
                JsonObject object = JsonObject.create();
                String id = "StateGroup::" + csvRow[0];
                // construct JSON object to insert into bucket
                object.put("id", id)
                        .put("State", csvRow[0])
                        .put("StateName", csvRow[1])
                        .put("GroupId", csvRow[2])
                        // CouchBase common info
                        .put("_class", this.className);
                // metaData
                JsonObject metaData = JsonObject.create();
                metaData.put("subCategory", "STGRP")
                        .put("createdBy", "migrationadmin")
                        .put("modifiedDate", curTime)
                        .put("modifiedBy", "migrationadmin")
                        .put("category", "CAP")
                        .put("creationDate", curTime);
                object.put("metaData", metaData);
                // create document
                JsonDocument document = JsonDocument.create(id, object);
                this.bucket.upsert(document);
                idSet.add(id);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("##################Count of StateGroup CSV distinct id: " + idSet.size() + "##################");
            logger.info("##################StateGroup CSV loaded to Couchbase!##################");
        }
    }

    /**
     * load Zipcodes CSV (in production) To Couchbase
     */
    public void loadZipcodesCSV(String csvFilePath) throws FileNotFoundException {
        loadZipcodesCSV(new BufferedReader(new FileReader(csvFilePath)));
    }

    public void loadZipcodesCSV(BufferedReader br) {
        // count distinct id
        HashSet<String> idSet = new HashSet<String>();

        try {
            this.csvReader = new CSVReader(br);
            String[] csvRow = null;

            long counter = 0;

            while ((csvRow = this.csvReader.readNext()) != null) {
                ++counter;
                if (counter % 1000 == 0) {
                    logger.info("~~~~~~~~~~~~~~~~~~~[loadZipcodesCSV counter]: " + counter);
                }

                long curTime = System.currentTimeMillis();
                JsonObject object = JsonObject.create();
                String id = "Zipcode::" + csvRow[0];
                // construct JSON object to insert into bucket
                object.put("id", id)
                        .put("ZipCode", csvRow[0])
                        .put("State", csvRow[1])
                        .put("State2", "StateGroup::" + csvRow[1])
                        .put("StateName", csvRow[2])
                        .put("County", csvRow[3])
                        .put("FIPS", csvRow[4])
                        .put("ApplicableYear", csvRow[5])
                        // CouchBase common info
                        .put("_class", this.className);
                // metaData
                JsonObject metaData = JsonObject.create();
                metaData.put("subCategory", "ZIPCODE")
                        .put("createdBy", "migrationadmin")
                        .put("modifiedDate", curTime)
                        .put("modifiedBy", "migrationadmin")
                        .put("category", "CAP")
                        .put("creationDate", curTime);
                object.put("metaData", metaData);
                // create document
                JsonDocument document = JsonDocument.create(id, object);
                this.bucket.upsert(document);
                idSet.add(id);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("##################Count of Zipcodes CSV distinct id: " + idSet.size() + "##################");
            logger.info("##################Zipcodes CSV loaded to Couchbase!##################");
        }
    }

    /**
     * Zipcodes out of territory CSV loaded to Couchbase
     */
    public void loadZipcodesOutOfTerritoryCSV(String csvFilePath) throws FileNotFoundException {
        loadZipcodesOutOfTerritoryCSV(new BufferedReader(new FileReader(csvFilePath)));
    }

    public void loadZipcodesOutOfTerritoryCSV(BufferedReader br) {
        // count distinct id
        HashSet<String> idSet = new HashSet<String>();

        try {
            this.csvReader = new CSVReader(br);
            String[] csvRow = null;
            long counter = 0;

            while ((csvRow = this.csvReader.readNext()) != null) {
                ++counter;
                if (counter % 1000 == 0) {
                    logger.info("<<<<<<<<<<<<<<<<<<<<[loadZipcodesOutOfTerritoryCSV counter]: " + counter);
                }
                long curTime = System.currentTimeMillis();
                JsonObject object = JsonObject.create();
                String id = "ZipcodeNotInTerritory::" + csvRow[0];
                // construct JSON object to insert into bucket
                object.put("id", id)
                        .put("ZipCode", csvRow[0])
                        .put("State", csvRow[1])
                        .put("StateName", csvRow[2])
                        .put("County", csvRow[3])
                        .put("FIPS", csvRow[4])
                        .put("ApplicableYear", csvRow[5])
                        // CouchBase common info
                        .put("_class", this.className);
                // metaData
                JsonObject metaData = JsonObject.create();
                metaData.put("subCategory", "ZIPCODE_OUT_OF_TERRITORY")
                        .put("createdBy", "migrationadmin")
                        .put("modifiedDate", curTime)
                        .put("modifiedBy", "migrationadmin")
                        .put("category", "CAP")
                        .put("creationDate", curTime);
                object.put("metaData", metaData);
                // create document
                JsonDocument document = JsonDocument.create(id, object);
                this.bucket.upsert(document);
                idSet.add(id);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("##################Count of Zipcodes out of territory CSV distinct id: " + idSet.size() + "##################");
            logger.info("##################Zipcodes out of territory CSV loaded to Couchbase!##################");
        }
    }

}
